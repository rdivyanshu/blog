# Blogs
### Auguest 2023
- [Stuttering Steps and Temporal Formulas in TLA+](https://rdivyanshu.github.io/temporal.html)

### July 2023
- [Encoding Hitori in Answer Set Programming](https://rdivyanshu.github.io/asp.html)

### April 2023
- [Another verified program in Dafny](https://rdivyanshu.github.io/permutation.html)
- [A bit on Quantile Sketching](https://rdivyanshu.github.io/histogram.html) 

### July 2021
- [A verified program in Dafny](https://rdivyanshu.github.io/dafny.html) 

### June 2021
- [Solving  a mathemattic problem using Rosette](https://rdivyanshu.github.io/ma122.html)

